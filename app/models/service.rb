class Service < ActiveRecord::Base
  include ScopeBased

  has_attached_file :cover, styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :cover, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
