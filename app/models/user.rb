class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :timeoutable, :authentication_keys => [:login]

  has_one :profile, foreign_key: 'user_id', dependent: :destroy
  accepts_nested_attributes_for :profile, reject_if: :all_blank, allow_destroy: true

  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true

  has_many :products, foreign_key: 'user_id'
  has_many :blogs, foreign_key: 'user_id'

  include ScopeBased
  include TheUser::UserAuthenticate

  validates_uniqueness_of :username, :email
  validates :email, presence: true

  after_initialize :after_initialized
  before_save :downcase_username

  def is_admin?
    self.type == 'Admin'
  end

  private
    def downcase_username
      self.username = self.username.downcase if self.username_changed?
    end

    def after_initialized
      self.profile = Profile.new if self.profile.blank?
      self.address = Address.new if self.address.blank?
    end
end
