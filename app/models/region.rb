class Region < ActiveRecord::Base
  include ScopeBased
  include Tree
  scope :alfa, ->{order(name: :asc)}
end
