class Article < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  belongs_to :user
  
  include ScopeBased
  include TheSearch::ArticleSearch
  scope :activated, ->{where(status: true)}

  has_attached_file :cover, styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :cover, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  def status?
    if self.status == true
      return '<label class="label label-success">Already Publish</label>'.html_safe
    else
      return '<label class="label label-danger">Pending</label>'.html_safe
    end
  end
end
