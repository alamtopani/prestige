module TheSearch
  module LandingPageSearch
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(landing_pages.title) LIKE LOWER(:key)",
          "LOWER(landing_pages.category) LIKE LOWER(:key)"
        ].join(" OR ")
        where(query_opts, {key: "%#{_key}%"} )
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        return results
      end
    end
  end
end