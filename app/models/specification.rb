class Specification < ActiveRecord::Base
  belongs_to :specificationable, polymorphic: true
  
  include ScopeBased
  default_scope {order(created_at: :desc)}
end
