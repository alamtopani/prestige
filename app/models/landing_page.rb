class LandingPage < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  
  include ScopeBased
  include TheSearch::LandingPageSearch
end
