class Product < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  before_create :prepare_code
  after_initialize :populate_galleries, :populate_overview

  include ScopeBased
  include TheSearch::ProductSearch
  
  scope :activated, ->{where(status: true)}

  belongs_to :user, foreign_key: 'user_id'

  has_many :specifications, as: :specificationable, dependent: :destroy
  accepts_nested_attributes_for :specifications, reject_if: :all_blank, allow_destroy: true

  has_many :galleries, as: :galleriable, dependent: :destroy
  accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

  has_attached_file :interior_image, styles: {
                      large:    '1200>',
                      small:    '300>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :interior_image, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :video

  def build_image(media_title, position=nil)
    gallery = self.galleries.build({title: media_title})
    gallery.position = position
  end

  def build_overview(media_title)
    specification = self.specifications.build({description: media_title})
  end

  def status?
    if self.status == true
      return '<label class="label label-success">Already Publish</label>'.html_safe
    else
      return '<label class="label label-danger">Pending</label>'.html_safe
    end
  end

  private
    def prepare_code
      self.code = SecureRandom.hex(3) if self.code.blank?
    end

    def populate_galleries
      if self.galleries.length < 4
        [
          'Cover',
          'Image1',
          'Image2',
          'Image3'
        ].each_with_index do |media_title, index|
          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end if self.new_record?
    end

    def populate_overview
      if self.specifications.length < 4
        [
          'Overview 1',
          'Overview 2',
          'Overview 3',
          'Overview 4'
        ].each_with_index do |media_title, index|
          _overview = self.specifications.select{|g| g.description.to_s.downcase == media_title.downcase}.first
          unless _overview
            self.build_overview(media_title)
          end
        end
      end if self.new_record?
    end
end
