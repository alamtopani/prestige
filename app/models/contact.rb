class Contact < ActiveRecord::Base
  include ScopeBased
  include TheSearch::ContactSearch

  scope :already_check, ->{where(status: true)}
  scope :not_ready_check, ->{where(status: false)}


  def status?
    if self.status == true
      return '<label class="label label-success">Already Check</label>'.html_safe
    else
      return '<label class="label label-danger">New</label>'.html_safe
    end
  end
end
