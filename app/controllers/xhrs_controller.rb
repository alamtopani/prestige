class XhrsController < FrontendController
  def cities
    province = Region.find_by(name: params[:id])
    @cities = province ? province.children : Region.none
    render layout: false
  end
end
