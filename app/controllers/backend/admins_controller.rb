class Backend::AdminsController < Backend::ApplicationController
  defaults resource_class: Admin, collection_name: 'admins', instance_name: 'admin'
  include UserFilter

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Admins", :collection_path

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
  end

  def new
    add_breadcrumb "New"
    super
  end

  def edit
    add_breadcrumb "Edit"
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end