class Backend::ArticlesController < Backend::ApplicationController
  defaults resource_class: Article, collection_name: 'articles', instance_name: 'article'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Articles", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def new
    add_breadcrumb "New"
    super
  end

  def edit
    add_breadcrumb "Edit"
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
