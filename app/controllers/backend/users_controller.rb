class Backend::UsersController < Backend::ApplicationController
  defaults resource_class: User, collection_name: 'users', instance_name: 'user'
  include UserFilter

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Users", :collection_path

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end