class Backend::ApplicationController < Backend::ResourcesController
  layout 'backend'
  before_filter :authenticate_admin!, :prepare_action
  protect_from_forgery with: :exception

  def authenticate_admin!
    unless current_user.present? && (current_user.is_admin?)
      redirect_to backend_dashboard_path, alert: "Can't Access this page"
    end
  end

  protected
    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end

    def prepare_action
      @messages = Contact.latest.limit(5)
    end
end