class Backend::WebSettingsController < Backend::ApplicationController
  defaults resource_class: WebSetting, collection_name: 'web_settings', instance_name: 'web_setting'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Web Settings", :collection_path

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def new
    add_breadcrumb "New"
    super
  end

  def edit
    add_breadcrumb "Edit"
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
