class Backend::ServicesController < Backend::ApplicationController
  defaults resource_class: Service, collection_name: 'services', instance_name: 'service'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Services", :collection_path

  include MultipleAction
  
  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def new
    add_breadcrumb "New"
    super
  end

  def edit
    add_breadcrumb "Edit"
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
