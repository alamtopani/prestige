class Backend::ContactsController < Backend::ApplicationController
  defaults resource_class: Contact, collection_name: 'contacts', instance_name: 'contact'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Contacts", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def new
    add_breadcrumb "New"
    super
  end

  def edit
    add_breadcrumb "Edit"
  end

  def show
    add_breadcrumb "Show"
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
