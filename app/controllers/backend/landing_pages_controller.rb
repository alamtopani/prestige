class Backend::LandingPagesController < Backend::ApplicationController
  defaults resource_class: LandingPage, collection_name: 'landing_pages', instance_name: 'landing_page'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Landing Page", :collection_path

  include MultipleAction
  
  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def new
    add_breadcrumb "New"
    super
  end

  def edit
    add_breadcrumb "Edit"
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
