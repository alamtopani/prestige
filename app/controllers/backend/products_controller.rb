class Backend::ProductsController < Backend::ApplicationController
  defaults resource_class: Product, collection_name: 'products', instance_name: 'product'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Products", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def download
    @product = Product.find params[:id]
    url = @product.video.url
    file_path = url.split("?").first
    if !file_path.nil?
      send_file "#{Rails.root}/public#{file_path}", :x_sendfile => true 
    else 
      redirect_to :back
    end
  end

  def new
    add_breadcrumb "New"
    super
  end

  def edit
    add_breadcrumb "Edit"
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
