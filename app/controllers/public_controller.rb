class PublicController < ApplicationController

	def home
	end

	def about
	end

	def contact_us
		render layout: 'application_page'
	end

	def product_v1
		render layout: 'application_page'
	end

	def product_v2
		render layout: 'application_page'
	end

	def product_single_v1
		render layout: 'application_page'
	end

	def product_single_v2
		render layout: 'application_page'
	end

	def news_event_v1
		render layout: 'application_page'
	end

	def news_event_v2
		render layout: 'application_page'
	end

	def news_event_single
		render layout: 'application_page'
	end

	def video_blog
		render layout: 'application_page'
	end

	def video_blog_single
		render layout: 'application_page'
	end
end
