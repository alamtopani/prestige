/*
* @author     : Fathan Rohman
* @company    : Creative Design Sukabumi - www.creativedesignsukabumi.com
* @production : 6 Oktober 2015 - Start At 07:45-00:00:27 PM
*/

/*
*
* FEATURE THEME JAVASCRIPT(JS)
*
*
*- SCOPE ELEMENT
*/

$(window).load(function() {
  // ISOTOPE
  var $container = $('.isotope-container');
  $container.isotope({
    filter: '*',
    animationOptions: {
      duration: 750,
      easing: 'linear',
      queue: false
    }
  });

  $('.filters a').click(function(){
    var selector = $(this).attr('data-filter');
    $container.isotope({
      filter: selector,
      animationOptions: {
        duration: 750,
        easing: 'linear',
        queue: false
      }
    });
    return false;
  });

  // PRELOADER
  $("#preloader").delay(1000).fadeOut("slow");

  // CAROUSEL VIDEO DETAIL
  var boxheight = $('#myCarousel .carousel-inner').innerHeight();
  var itemlength = $('#myCarousel .item').length;
  var triggerheight = Math.round(boxheight/itemlength+1);
  $('#myCarousel .list-group-item').outerHeight(triggerheight);

  // CAROUSEL PRODUCT CAR DETAIL
  $(window).resize(function(){
    $('.home-carousel .item').each(function(){
      var topSlider = $('.home-carousel').outerWidth() / 2.5;
      $(this).css('height', topSlider);
    });
  }).resize();

  // CAROUSEL RECENT PRODUCT 3 IMAGE
  $('.recent-product-3-item').each(function(i) {
    if( i % 3 == 0 ) {
      $(this).nextAll().andSelf().slice(0,3).wrapAll('<div class="item"></div>');
    }
  });
  $('.carousel_recent_product_3 .item:first-child').addClass('active');

  // CAROUSEL RECENT PRODUCT 5 IMAGE
  $('.recent-product-5-item').each(function(i) {
    if( i % 5 == 0 ) {
      $(this).nextAll().andSelf().slice(0,5).wrapAll('<div class="item"></div>');
    }
  });
  $('.carousel_recent_product_5 .item:first-child').addClass('active');
});

$(document).ready(function(){
  // SCROLL TO FIXED MENU TOP
  $(function(){
    $('.menu-fixed-top').scrollToFixed();
  });

  // SCROLL BACK TO TOP
  $(function ScrollTopUp(){
    var offset=220;
    var duration=1000;
    jQuery(window).scroll(function(){
      if(jQuery(this).scrollTop()>offset){
        jQuery('.backtotop').css({opacity:"1",display:"block",});
      }else{
        jQuery('.backtotop').css('opacity','0');
      }
    });

    jQuery('.backtotop').click(function(event){
      event.preventDefault();
      jQuery('html, body').animate({scrollTop:0},duration);
      return false;
    });
  });

  // FORM SEARCH FULL
  $(function () {
    $('a[href="#search"]').on('click', function(event) {
      event.preventDefault();
      $('#search').addClass('open');
      $('#search > form > input[type="search"]').focus();
    });

    $('#search, #search button.close').on('click keyup', function(event) {
      if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
        $(this).removeClass('open');
      }
    });

    $('form').submit(function(event) {
      event.preventDefault();
      return false;
    })
  });

  // NICESCROLL
  // $(function () {
  //   $("html").niceScroll();
  // });

  // GRID INIT
  $(function() {
    Grid.init();
  });

  // VIDEO MAGNIFIC POPUP
  $('.video-popup-magnific').magnificPopup({
    type: 'iframe',
  });

  // PARALLAX VIDEO BACKGROUND
  $(function(){
    $('#my-video').backgroundVideo({
      pauseVideoOnViewLoss: false,
      parallaxOptions: {
        effect: 1.9
      }
    });
  });

  // PARALLAX SLIDER REVOLUTION CUSTOM
  $(function(){
    // $('modern-slider-prestigecar')
  });

  // GMAPS3
  $(function(){
    $('#maps').gmap3({
      map:{
        options:{
          center:[-6.110832,106.780028],
          zoom: 15,
          scrollwheel: false
        }
      },
      marker:{
        values:[
        {latLng:[-6.110832,106.780028], data:"Prestige Image Motorcars"},
        ],
        options:{
          draggable: false,
          scrollwheel: false
        },
        events:{
          mouseover: function(marker, event, context){
            var map = $(this).gmap3("get"),
            infowindow = $(this).gmap3({get:{name:"infowindow"}});
            if (infowindow){
              infowindow.open(map, marker);
              infowindow.setContent(context.data);
            } else {
              $(this).gmap3({
                infowindow:{
                  anchor:marker,
                  options:{content: context.data}
                }
              });
            }
          },
          mouseout: function(){
            var infowindow = $(this).gmap3({get:{name:"infowindow"}});
            if (infowindow){
              infowindow.close();
            }
          }
        }
      }
    });
  });

  // SLIDER CAROUSEL VIDEO BLOG DETAIL
  var clickEvent = false;
  $('#myCarousel').carousel({
    interval:   4000
  }).on('click', '.list-group li', function() {
    clickEvent = true;
    $('.list-group li').removeClass('active');
    $(this).addClass('active');
  }).on('slid.bs.carousel', function(e) {
    if(!clickEvent) {
      var count = $('.list-group').children().length -1;
      var current = $('.list-group li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if(count == id) {
        $('.list-group li').first().addClass('active');
      }
    }
    clickEvent = false;
  });

  // PINTEREST GRID
  $('#pinBoot').pinterest_grid({
    no_columns: 4,
    padding_x: 10,
    padding_y: 10,
    margin_bottom: 50
  });
});

(function($) {
  "use strict";
  //REVULUTION SLIDER
  if($('.main-slider .tp-banner').length){
    jQuery('.main-slider .tp-banner').show().revolution({
      delay:10000,
      startwidth:1200,
      startheight:620,
      hideThumbs:600,

      thumbWidth:80,
      thumbHeight:50,
      thumbAmount:5,

      navigationType:"bullet",
      navigationArrows:"0",
      navigationStyle:"preview4",

      touchenabled:"on",
      onHoverStop:"off",

      swipe_velocity: 0.7,
      swipe_min_touches: 1,
      swipe_max_touches: 1,
      drag_block_vertical: false,

      parallax:"mouse",
      parallaxBgFreeze:"on",
      parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

      keyboardNavigation:"off",

      navigationHAlign:"center",
      navigationVAlign:"bottom",
      navigationHOffset:0,
      navigationVOffset:20,

      soloArrowLeftHalign:"left",
      soloArrowLeftValign:"center",
      soloArrowLeftHOffset:20,
      soloArrowLeftVOffset:0,

      soloArrowRightHalign:"right",
      soloArrowRightValign:"center",
      soloArrowRightHOffset:20,
      soloArrowRightVOffset:0,

      shadow:0,
      fullWidth:"on",
      fullScreen:"off",

      spinner:"spinner4",

      stopLoop:"off",
      stopAfterLoops:-1,
      stopAtSlide:-1,

      shuffle:"off",

      autoHeight:"off",
      forceFullWidth:"on",

      hideThumbsOnMobile:"on",
      hideNavDelayOnMobile:1500,
      hideBulletsOnMobile:"on",
      hideArrowsOnMobile:"on",
      hideThumbsUnderResolution:0,

      hideSliderAtLimit:0,
      hideCaptionAtLimit:0,
      hideAllCaptionAtLilmit:0,
      startWithSlide:0,
      videoJsPath:"",
      fullScreenOffsetContainer: ".main-slider"
    });
  }
})(window.jQuery);

jQuery('.main nav li a[href^="#"], .scroll-to, .btn[href^="#"]').on('click', function(){
  var navHeight = jQuery('.main-nav').outerHeight();
  var anchor = jQuery(this).attr('href');
  jQuery('html, body').animate({
    scrollTop: jQuery(anchor).offset().top - navHeight
  }, 800);
  return false;
});

// MENU ON SCROLL CHANGE
$(window).scroll(function(){
  var scroll = $(window).scrollTop();
  if (scroll > 0) {
    $('.main-nav').addClass('nav-colored');
  }else{
    $('.main-nav').removeClass('nav-colored');
  };
});
