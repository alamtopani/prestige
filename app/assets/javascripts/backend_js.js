//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require cocoon
//= require ahoy
//= require ckeditor/init
//= require ckeditor/config

$(document).ready(function() {
  $('.provinces_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });
});
