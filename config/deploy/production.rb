role :app, %w{prestigecars@63.142.252.102}
role :web, %w{prestigecars@63.142.252.102}
role :db,  %w{prestigecars@63.142.252.102}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '63.142.252.102', user: 'prestigecars', roles: %w{web app}

set :rbenv_type,     :system
set :rbenv_ruby,     '2.1.2'
set :rbenv_prefix,   "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles,    :all

set :deploy_to,       "/home/prestigecars/www"
set :rails_env,       "production"
set :branch,          "master"

set :unicorn_config_path, "/home/prestigecars/www/current/config/unicorn.rb"
