Rails.application.routes.draw do

  # FRONTEND IN STAGING SERVER
  # scope '/prestige' do
  #   get 'about', to: 'public#about', as: 'about'
  #   get 'product_v1', to: 'public#product_v1', as: 'product_v1'
  #   get 'product_v2', to: 'public#product_v2', as: 'product_v2'
  #   get 'product_single_v1', to: 'public#product_single_v1', as: 'product_single_v1'
  #   get 'product_single_v2', to: 'public#product_single_v2', as: 'product_single_v2'
  #   get 'contact_us', to: 'public#contact_us', as: 'contact_us'
  #   get 'news_event_single', to: 'public#news_event_single', as: 'news_event_single'
  #   get 'news_event_v2', to: 'public#news_event_v2', as: 'news_event_v2'
  #   get 'news_event_v1', to: 'public#news_event_v1', as: 'news_event_v1'
  #   get 'video_blog', to: 'public#video_blog', as: 'video_blog'
  #   get 'video_blog_single', to: 'public#video_blog_single', as: 'video_blog_single'
  #   get '/', to: "public#home", as: 'root'
  # end

  devise_for :users,
  controllers: {
    registrations: 'registrations',
    sessions: 'sessions'
  },
  path_names: {
    sign_in:  'login',
    sign_out: 'logout',
    sign_up:  'register'
  }

  namespace :backend do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'

    resources :users
    resources :admins
    resources :articles do
      collection do
        post :do_multiple_act
      end
    end
    resources :blogs do
      collection do
        post :do_multiple_act
      end
    end
    resources :products do
      collection do
        post :do_multiple_act
        get :download
      end
    end
    resources :contacts do
      collection do
        post :do_multiple_act
      end
    end
    resources :landing_pages do
      collection do
        post :do_multiple_act
      end
    end
    resources :services do
      collection do
        post :do_multiple_act
      end
    end
    resources :web_settings
  end

  resources :xhrs do
    collection do
      get :cities
    end
  end

  root 'public#home'
  get 'home', to: 'public#home', as: 'home'
  get 'about', to: 'public#about', as: 'about'
  get 'product_v1', to: 'public#product_v1', as: 'product_v1'
  get 'product_v2', to: 'public#product_v2', as: 'product_v2'
  get 'product_single_v1', to: 'public#product_single_v1', as: 'product_single_v1'
  get 'product_single_v2', to: 'public#product_single_v2', as: 'product_single_v2'
  get 'contact_us', to: 'public#contact_us', as: 'contact_us'
  get 'news_event_single', to: 'public#news_event_single', as: 'news_event_single'
  get 'news_event_v2', to: 'public#news_event_v2', as: 'news_event_v2'
  get 'news_event_v1', to: 'public#news_event_v1', as: 'news_event_v1'
  get 'video_blog', to: 'public#video_blog', as: 'video_blog'
  get 'video_blog_single', to: 'public#video_blog_single', as: 'video_blog_single'
  # get '/', to: "public#home", as: 'root'

end
