module Permitable
  USER = [
    :id,
    :type,
    :username,
    :email,
    :password,
    :password_confirmation
  ]

  PROFILE = {
    profile_attributes: [
      :id,
      :user_id,
      :full_name,
      :birthday,
      :gender,
      :avatar
    ]
  }

  ADDRESS = {
    address_attributes: [
      :id,
      :address,
      :city,
      :province,
      :postcode,
      :addressable_type,
      :addressable_id,
      :addressable
    ]
  }

  REGION = [
    :id,
    :name,
    :ancestry,
    :parent_id
  ]

  PRODUCT = [
    :id,
    :code,
    :title,
    :slug,
    :category,
    :description,
    :interior_image,
    :video,
    :price_on_the_road,
    :price_of_the_road,
    :user_id,
    :status
  ]

  SPECIFICATION = {
    specifications_attributes: [
      :id,
      :description,
      :specificationable_type,
      :specificationable_id,
      :specificationable,
      :_destroy
    ]
  }

  GALLERY = {
    galleries_attributes: [
      :id,
      :title,
      :description,
      :file,
      :galleriable_type,
      :galleriable_id,
      :galleriable,
      :position,
      :_destroy
    ]
  }

  BLOG = [
    :id,
    :title,
    :slug,
    :description,
    :user_id,
    :category,
    :cover,
    :url_video,
    :status
  ]

  ARTICLE = [
    :id,
    :title,
    :slug,
    :description,
    :user_id,
    :category,
    :cover,
    :status,
    :event_at
  ]

  CONTACT = [
    :id,
    :full_name,
    :email,
    :phone,
    :company,
    :address,
    :subject,
    :message,
    :status
  ]

  LANDING_PAGE = [
    :title,
    :slug,
    :description,
    :status,
    :category
  ]

  SERVICE = [
    :id,
    :title,
    :description,
    :cover
  ]

  WEB_SETTING = [
    :id,
    :header_tags,
    :footer_tags,
    :contact,
    :email,
    :favicon,
    :logo,
    :facebook,
    :twitter,
    :title,
    :keywords,
    :description,
    :robot,
    :author,
    :copyright
  ]

  def self.controller(name)
    self.send name.gsub(/\W/,'_').singularize.downcase
  end

  # ROLE ADMIN ---------------------------------------------------

  def self.backend_user
    USER.dup.push(PROFILE.dup).push(ADDRESS.dup)
  end

  def self.backend_admin
    backend_user
  end

  def self.backend_blog
    BLOG
  end

  def self.backend_article
    ARTICLE
  end

  def self.backend_product
    PRODUCT.dup.push(GALLERY.dup).push(SPECIFICATION.dup)
  end

  def self.backend_contact
    CONTACT
  end

  def self.backend_landing_page
    LANDING_PAGE
  end

  def self.backend_service
    SERVICE
  end

  def self.backend_web_setting
    WEB_SETTING.dup.push(GALLERY.dup)
  end

end
