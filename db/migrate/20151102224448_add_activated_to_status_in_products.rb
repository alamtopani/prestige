class AddActivatedToStatusInProducts < ActiveRecord::Migration
  def change
    rename_column :products, :activated, :status
    rename_column :blogs, :activated, :status
    rename_column :articles, :activated, :status
  end
end
