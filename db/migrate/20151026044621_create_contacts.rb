class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :full_name
      t.string :email
      t.string :phone
      t.string :company
      t.text :address
      t.string :subject
      t.string :message
      t.boolean :status, default: false

      t.timestamps null: false
    end
  end
end
