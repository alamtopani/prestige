class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.integer :user_id
      t.string :category
      t.attachment :cover
      t.boolean :activated
      t.date :event_at

      t.timestamps null: false
    end
  end
end
