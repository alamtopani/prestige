class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :full_name
      t.date :birthday
      t.string :gender
      t.integer :user_id
      t.attachment :avatar

      t.timestamps null: false
    end
  end
end
