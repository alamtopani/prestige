class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.integer :user_id
      t.string :category
      t.attachment :cover
      t.string :url_video
      t.boolean :activated, default: false

      t.timestamps null: false
    end

    add_index :blogs, :user_id
  end
end
