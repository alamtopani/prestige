class ChangeNameSpecSpecificationable < ActiveRecord::Migration
  def change
    rename_column :specifications, :specification_id, :specificationable_id
    rename_column :specifications, :specification_type, :specificationable_type
  end
end
