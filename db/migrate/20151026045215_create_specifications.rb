class CreateSpecifications < ActiveRecord::Migration
  def change
    create_table :specifications do |t|
      t.text :description
      t.string :specification_type
      t.integer :specification_id

      t.timestamps null: false
    end

    add_index :specifications, :specification_id
  end
end
