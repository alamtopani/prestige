class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :slug
      t.string :category
      t.text :description
      t.attachment :interior_image
      t.attachment :video
      t.decimal :price_on_the_road
      t.decimal :price_of_the_road
      t.string :code
      t.integer :user_id
      t.boolean :activated, default: false

      t.timestamps null: false
    end

    add_index :products, :user_id
  end
end
