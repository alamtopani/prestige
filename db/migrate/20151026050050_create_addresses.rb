class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :province
      t.string :city
      t.text :address
      t.string :postcode
      t.string :addressable_type
      t.integer :addressable_id

      t.timestamps null: false
    end
  end
end
