module SeedUser
  def self.seed
    user = User.find_or_initialize_by(email: "admin@example.com")
    user.username = 'adminmaster'
    user.password = 12345678
    user.password_confirmation = 12345678
    user.type = 'Admin'
    user.confirmation_token = nil
    user.confirmed_at = Time.now
    user.save
  end
end
