module SeedWebSetting
  def self.seed 
    setting = WebSetting.find_or_initialize_by(email: 'prestigecar@gmail.com')
    setting.title = "PrestigeCar"
    setting.description =  "Learn creative and technical skills with Tuts+ video courses and free tutorials. Topics include code, design, photography and more."
    setting.keywords =  "Html Template, Responsive Theme, Responsive Wordpress, Magento Template"
    setting.header_tags =  'PrestigeCar'
    setting.footer_tags =  '© 2015 Prestige Car Indonesia'
    setting.contact =  '0266239880'
    setting.facebook =  'http://facebook.com'
    setting.twitter =  'http://facebook.com'
    setting.logo =  File.open("#{Rails.root}/db/assets/logo.jpg")
    setting.favicon =  File.open("#{Rails.root}/db/assets/logo.jpg")
    setting.save
  end
end